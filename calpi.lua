local unpack = unpack or table.unpack

local function calpi()
	local op = true
	local operator = "+"
	local mypi = 0
	for i=1, 10000000000, 2 do
		mypi, op = (op and mypi+(4/i) or mypi-(4/i)), (not op and true or false)
	end
end

start = os.clock()
calpi()
stop = os.clock()

return (stop - start)
